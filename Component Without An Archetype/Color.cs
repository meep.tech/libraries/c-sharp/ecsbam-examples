﻿using Meep.Tech.Data.Extensions;
using System;
using Meep.Tech.Data.Base;
using Meep.Tech.Data.Serialization;
using System.Diagnostics.CodeAnalysis;


namespace Meep.Tech.Data.Examples {

  [Serializable]
  public struct Color : IModelDataComponent<Color> {

    /// <summary>
    /// The default quantity of stacks
    /// </summary>
    public const string DefaultColorValue = "Red";

    /// <summary>
    /// Id for this component's basic archetype:
    /// </summary>
    public static Model.DataComponent.Id Id {
      get;
    } = new Model.DataComponent.Id(
      "Item Color",
      typeof(Color)
    );

    /// <summary>
    /// Color related params
    /// </summary>
    public class Params : Param {

      /// <summary>
      /// The color to make the item stack with
      /// </summary>
      public static Param Color
        = new Params("Color", typeof(int?));

      Params(string name, Type type)
        : base(name, type) { }
    }

    /// <summary>
    /// The current stack qty of this item
    /// </summary>
    public string value {
      get;
      internal set;
    }

    /// <summary>
    /// Ctor used by the link system
    /// </summary>
    Color(Model.Params provided, Serializer.SerializedData? serialized)
      : this((provided, serialized).TryToGetAs(Params.Color, "value", DefaultColorValue)) {}

    /// <summary>
    /// Ctor used by the link system
    /// </summary>
    public Color(string colorValue) {
      value = colorValue;
    }

    public bool Equals([AllowNull] Color other)
      => other.value == value;

    public override bool Equals(object obj) {
      return obj is Color otherColor
        && value == otherColor.value;
    }

    Model.DataComponent.Type<Color> IModelDataComponent<Color>.type {
      get => null;
      set => value = null;
    }
  }
}