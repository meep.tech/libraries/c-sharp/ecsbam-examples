﻿using Meep.Tech.Data.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meep.Tech.Data.Examples {

  /// <summary>
  /// Archetype for making colored items
  /// </summary>
  public class ColoredItem : Item.Type {

    public new static Item.Id Id {
      get;
    } = new Item.Id("Colored");

    public override OrderedSet<Model.DataComponent.Id> DefaultModelDataComponents 
      => base.DefaultModelDataComponents
        .Append(Color.Id);

    ColoredItem() 
      : base(Id) {}

    public Item Make(string colorValue)
      => Make((Color.Params.Color, colorValue));
  }
}
