﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using System.Collections.Generic;

namespace Meep.Tech.Data.Examples {

  /// <summary>
  /// How worn out this weapon is:
  /// </summary>
  [Attributes.Components.ComponentDataModel]
  public class WeaponWear : Model.DataComponent<WeaponWear>, IFullySerializeableComponent {

    [Attributes.Components.ComponentDataField]
    public int wearLevel
      = 1;

    [Attributes.Components.ComponentNumberSqlDataFieldOne]
    public int random
      = 12312;

    [field: Attributes.Components.ComponentOwnerField]
    string IOwned.owner { 
      get; 
      set;
    }

    public override bool Equals(WeaponWear other)
      => wearLevel == other.wearLevel && random == other.random;

    public WeaponWear(int? wearLevel = null) 
      : base(Type.Id.Archetype as Model.DataComponent.Type<WeaponWear>) {
      this.wearLevel = wearLevel ?? this.wearLevel;
    }

    public sealed class Type : Model.DataComponent.Type<WeaponWear> {
      public static Model.DataComponent.Id Id {
        get; 
      } = new Model.DataComponent.Id("WeaponWear", "Items.Weapons");

      Type() 
        : base(Id) {}

      protected override WeaponWear InitializeModel(Model.Params @params = null)
        => new WeaponWear();
    }
  }
}
