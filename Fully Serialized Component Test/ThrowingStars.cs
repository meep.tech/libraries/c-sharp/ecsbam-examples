﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;

namespace Meep.Tech.Data.Examples {
  public class ThrowingStars : Weapon.Type {

    public static Item.Id Id {
      get;
    } = new Item.Id("Throwing Stars", "Weapons.");

    ThrowingStars() 
      : base(Id) {}

    public override OrderedSet<Model.DataComponent.Id> DefaultModelDataComponents 
      => base.DefaultModelDataComponents
        .Append(WeaponWear.Type.Id);

    public override OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent> DefaultSystemComponents
      => base.DefaultSystemComponents
          .Update(
            // id of the base component to update:
            WeaponStatsLinkSystemComponent.Type.Id,
            (WeaponStatsLinkSystemComponent existingLinkSystem) => new WeaponStatsLinkSystemComponent(
              new WeaponStatsLinkSystemComponent.ArchetypeData(damage: 2)
            )
          ).Append(
            new StackQuantityStackableLink(
              new Stackable(defaultStackQuantityOnMake: 10, maxStackQuantity: 50)
            )
          );
  }
}
