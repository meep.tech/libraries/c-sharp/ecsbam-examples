﻿namespace Meep.Tech.Data.Examples {

  public class Book : Item.Type {

    public static Item.Id Id { get; } = new Item.Id("Book");

    protected Book() : base(Book.Id) {}
  }
}
