﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;
using System.Collections.Generic;

namespace Meep.Tech.Data.Examples {

  public partial class Item {

    /// <summary>
    /// The type collection to hold and register all the Item.Types
    /// </summary>
    public static Archetype.Collection<Item, Item.Type, Item.Id> Types {
      get;
    } = new Archetype.Collection<Item, Type, Id>();

    /// <summary>
    /// The base Archetype Type class for Item Models.
    /// All item models must be made with a subtype of this Type as it's "type" value
    /// </summary>
    public abstract class Type : Archetype<Item, Item.Type, Item.Id> {

      /// <summary>
      /// Used for making new Item.Types
      /// </summary>
      /// <param name="id"></param>
      protected Type(Item.Id id)
        : base(id, Item.Types) {}

      /// <summary>
      /// Add a component to the models this archetype produces:
      /// </summary>
      public override OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent> DefaultSystemComponents
        => base.DefaultSystemComponents.Append(
          /// this should add all 3
          StackQuantityStackableLink.TypeId,
           new StackQuantityStackableLink(
             new Stackable(1, 100)
           )
        );

      /// <summary>
      /// Set up the constructor for the model:
      /// </summary>
      protected override Item InitializeModel(Model.Params @params)
        => new Item(this);
    }
  }
}
