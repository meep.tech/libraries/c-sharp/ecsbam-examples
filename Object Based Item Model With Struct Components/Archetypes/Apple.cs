﻿namespace Meep.Tech.Data.Examples {

  public class Apple : Item.Type {

    public static Item.Id Id { get; } = new Item.Id("Apple");

    Apple() : base(Apple.Id) {}
  }
}
