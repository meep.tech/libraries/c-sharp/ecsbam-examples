﻿using Meep.Tech.Data.Attributes.Models;
using Meep.Tech.Data.Base;

namespace Meep.Tech.Data.Examples {

  /// <summary>
  /// An item that could be used in a game:
  /// </summary>
  [DataModel("Item", typeof(Item.Type))]
  public partial class Item : Model<Item, Item.Type, Item.Id>, IUnique {

    /// <summary>
    /// Doesn't need to be abstract
    /// </summary>
    public class Id : Archetype.Identity {
      internal Id(string name, string externalIdPrefixEnding = "")
        : base(name, $"Items.{externalIdPrefixEnding}") { }
    }

    #region IUnique

    /// <summary>
    /// An id field to make each of these models unique
    /// </summary>
    [ModelIdField]
    string IUnique.id {
      get; 
      set; 
    }

    #endregion

    /// <summary>
    /// Build an item of the given archetype.
    /// This should only be used in InitializeModel.
    /// </summary>
    /// <param name="archetype"></param>
    protected Item(Item.Type archetype) : base(archetype) {}
  }
}
