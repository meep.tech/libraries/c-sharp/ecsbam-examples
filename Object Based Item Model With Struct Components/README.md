﻿# Item Archetype and Model with Stack Quantity added from a Mod as a Struct based Component

This example shows you a few things. For one it shows how to set up a simple archetype and model as seen in:
* Item.cs (Model)
* Item.Type.cs (Archetype)

You could also have them in the same file if you wanted to avoid a partial class, or you could have Item.Type be ItemType and not have it as a sub-class of Item at all if you really want.


It also explains how to create a component that has initialization logic based entirely on structs using components. This uses a Link System Component to connect the Archetype's singleton Stackable Data component to the Model's StackQuantity component, and uses defaults from the Archetype's component to set up the Model's component using the logic in the Link System Compoent.

This functionality could easily also be added in the Archetype class and model class itself as just fields and logic in FinalzeModel or Make under the archetype, but this example provides a more modular approach, and one that could also be applied via a mod to all Item.Types in existing mods.