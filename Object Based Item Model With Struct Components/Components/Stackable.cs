﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;

namespace Meep.Tech.Data.Examples {

  /// <summary>
  /// Indicates this archetype represents something stackable
  /// </summary>
  public struct Stackable : IArchetypeDataComponent<Stackable> {

    /// <summary>
    /// The max stack qty of this item
    /// </summary>
    public int NewStackQuantity {
      get;
    }

    /// <summary>
    /// The max stack qty of this item
    /// </summary>
    public int MaxStackQuantity {
      get;
    }

    /// <summary>
    /// The archetype of this model
    /// </summary>
    public Archetype.DataComponent.Type<Stackable> type {
      get;
      private set;
    }

    /// <summary>
    /// Basic CTOR for defining new Archetypes:
    /// </summary>
    public Stackable(int? defaultStackQuantityOnMake = null, int? maxStackQuantity = null) {
      type = Components.ArchetypeDataComponents.Get<Archetype.DataComponent.Type<Stackable>>(TypeId);
      Type stackableType = (type as Stackable.Type);
      MaxStackQuantity = maxStackQuantity ?? stackableType.DefaultMaxStackQuantity;
      NewStackQuantity = defaultStackQuantityOnMake ?? stackableType.DefaultNewStackQuantity;
    }

    /// <summary>
    /// Id for this component's basic archetype:
    /// </summary>
    public static Archetype.DataComponent.Id TypeId {
      get;
    } = new Archetype.DataComponent.Id("Stackable");

    /// <summary>
    /// Just a standard type for this:
    /// </summary>
    public class Type : Archetype.DataComponent.Type<Stackable> {

      /// <summary>
      /// The max stack qty of this item
      /// </summary>
      public int DefaultNewStackQuantity {
        get;
      } = 1;

      /// <summary>
      /// The max stack qty of this item
      /// </summary>
      public int DefaultMaxStackQuantity {
        get;
      } = 100;

      Type() : base(TypeId) { }
    }
  }
}
