﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Meep.Tech.Data.Examples {

  [Serializable]
  public struct StackQuantity
    : IModelDataComponent<StackQuantity> 
  {
    /// <summary>
    /// The current stack qty of this item
    /// </summary>
    public int currentStackQuantity {
      get;
      internal set;
    }

    /// <summary>
    /// Ctor used by the link system
    /// </summary>
    public StackQuantity(int currentStackQuantity) {
      type = Components.ModelDatas.Get<Model.DataComponent.Type<StackQuantity>>(TypeId);
      this.currentStackQuantity = currentStackQuantity;
    }

    public override bool Equals(object obj) {
      return obj is StackQuantity otherStackQuantity 
        && currentStackQuantity == otherStackQuantity.currentStackQuantity;
    }

    public bool Equals([AllowNull] StackQuantity other) 
      => other.currentStackQuantity == currentStackQuantity;

    #region Basic Component model Implimentations

    /// <summary>
    /// Id for this component's basic archetype:
    /// </summary>
    public static Model.DataComponent.Id TypeId {
      get;
    } = new Model.DataComponent.Id("StackQuantity");

    /// <summary>
    /// The archetype of this model
    /// </summary>
    public Model.DataComponent.Type<StackQuantity> type {
      get;
      private set;
    }

    Model.DataComponent.Type<StackQuantity> IModelDataComponent<StackQuantity>.type { 
      get => type; 
      set => type = value; 
    }

    /// <summary>
    /// Just a standard type for this:
    /// </summary>
    public class Type : Model.DataComponent.Type<StackQuantity> {

      public const int DefaultStackQuantity = 1;

      Type() : base(TypeId) { }

      protected override StackQuantity InitializeModel(Model.Params @params)
        => new StackQuantity(DefaultStackQuantity);
    }

    #endregion
  }
}
