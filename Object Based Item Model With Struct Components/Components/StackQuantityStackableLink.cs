﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;
using System.Collections.Generic;

namespace Meep.Tech.Data.Examples {

  /// <summary>
  /// System component for linking stack quantity
  /// </summary>
  public struct StackQuantityStackableLink : ILinkSystemComponent<
    StackQuantityStackableLink,
    StackQuantity,
    Stackable,
    StackQuantity.Type,
    Stackable.Type
  > {

    public Archetype.LinkSystemComponent.Type<StackQuantityStackableLink, StackQuantity, Stackable, StackQuantity.Type, Stackable.Type> type {
      get;
    } ILinkSystemComponentType ILinkSystemComponent.type 
      => type;

    public Stackable currentArchetypeDataComponent {
      get;
    }

    /// <summary>
    /// Constructor for Make
    /// </summary>
    /// <param name="type"></param>
    internal StackQuantityStackableLink(Type type) {
      this.type = type;
      this.currentArchetypeDataComponent = default;
    }

    /// <summary>
    /// Constructor for adding to types during initialization via Append etc
    /// </summary>
    /// <param name="stackableData"></param>
    public StackQuantityStackableLink(Stackable stackableData) {
      this.type = (Archetype.LinkSystemComponent.Type<StackQuantityStackableLink, StackQuantity, Stackable, StackQuantity.Type, Stackable.Type>)TypeId.Archetype;
      this.currentArchetypeDataComponent = stackableData;
    }

    /// <summary>
    /// Id for this component's basic archetype:
    /// </summary>
    public static Archetype.LinkSystemComponent.Id TypeId {
      get;
    } = new Archetype.LinkSystemComponent.Id("StackQuantityStackableLink");

    /// <summary>
    /// Just a standard type for this:
    /// </summary>
    public class Type : Archetype.LinkSystemComponent.Type<StackQuantityStackableLink, StackQuantity, Stackable, StackQuantity.Type, Stackable.Type> {

      Type() : base(TypeId) { }

      /// <summary>
      /// Make a stack quantity from an archetype stackable property:
      /// </summary>
      protected override StackQuantity InitializeModelComponentData(StackQuantity modelComponent, Stackable archetypeComponent, IArchetype archetype, IModel model, Model.Params @params) {
        return new StackQuantity(archetypeComponent.NewStackQuantity);
      }
    }
  }
}
