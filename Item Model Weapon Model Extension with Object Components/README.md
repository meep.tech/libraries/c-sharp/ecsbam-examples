﻿# Examples for How to Extend Base Models and Archetypes, and How to Enable Modability.
**This example extends the Item example under: "../Object Based Item Model With Struct Components"**

These examples help show how to extend existing types of models and archetypes, both within your own assembly/library/project/mod, and within future/external mods and libraries.