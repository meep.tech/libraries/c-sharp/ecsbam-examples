﻿namespace Meep.Tech.Data.Examples {
  public class WeaponDamageMultiplierModLinkType : WeaponStatsLinkSystemComponent.Type {

    public static WeaponStatsLinkSystemComponent.Id Id {
      get;
    } = new WeaponStatsLinkSystemComponent.Id("ModWeaponStats");

    protected WeaponDamageMultiplierModLinkType() 
      : base(Id) {}

    /// <summary>
    /// Lets make this do something different:
    /// </summary>
    public override SimpleLogic<int> ModdableCalculateDamageTotal { get; }
       = (modelComponent, archetypeComponent)
        => modelComponent.damageBonus * archetypeComponent.BaseDamageValue;
  }
}
