﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Loading;
using Meep.Tech.Data.Static;
using System.Collections.Generic;

namespace Meep.Tech.Data.Examples {
  public class WeaponStatsLinkSystemComponent : Archetype.LinkSystemComponent<
    WeaponStatsLinkSystemComponent,
    WeaponStatsLinkSystemComponent.ModelData,
    WeaponStatsLinkSystemComponent.ArchetypeData,
    WeaponStatsLinkSystemComponent.ModelData.Type,
    WeaponStatsLinkSystemComponent.ArchetypeData.Type
  > {

    #region Child Components

    /// <summary>
    /// The model's data component
    /// </summary>
    public class ModelData : Model.DataComponent<ModelData> {

      /// <summary>
      /// The current damage bonus
      /// </summary>
      public int damageBonus {
        get;
        internal set;
      } = 1;

      public static Model.DataComponent.Id Id {
        get;
      } = new Model.DataComponent.Id("Weapon Stat Data");

      protected ModelData()
        : base((Model.DataComponent.Type<ModelData>)Id.Archetype) { }

      #region Type

      public new class Type : Model.DataComponent.Type<ModelData> {

        #region Initialization

        protected Type() : base(ModelData.Id) { }

        #region Model Construction

        protected override ModelData InitializeModel(Model.Params @params)
          => new ModelData();

        #endregion

        #endregion
      }

      public override bool Equals(ModelData other)
        => other.damageBonus == damageBonus;

      #endregion
    }

    /// <summary>
    /// The archetype's data component
    /// </summary>
    public class ArchetypeData : Archetype.DataComponent<ArchetypeData> {

      public int BaseDamageValue {
        get;
        private set;
      }

      public static Archetype.DataComponent.Id Id {
        get;
      } = new Archetype.DataComponent.Id("Weapon Stat Data");

      public ArchetypeData(int? damage = null)
        : base((Archetype.DataComponent.Type<ArchetypeData>)Id.Archetype) 
      {
        // example of a way to use a default value
        BaseDamageValue = damage ?? (typeof(Type).AsArchetype() as Type).DefaultBaseDamageValue;
      }

      /// <summary>
      /// Before sealing (b4s_) you can change the base damage value:
      /// </summary>
      public void b4s_setBaseDamageValue(int newBaseDamageValue) {
        if (!ArchetypeLoader.IsSealed) {
          BaseDamageValue = newBaseDamageValue;
        } else throw new ArchetypeLoader.AttemptedUpdateWhileSealedException("B4s_setBaseDamageValue cannot be called after archetypes are sealed.");
      }

      #region Type

      public class Type : Archetype.DataComponent.Type<ArchetypeData> {

        // you can update this default vaue!
        public virtual int DefaultBaseDamageValue
          => 5;

        #region Initialization

        protected Type() : base(ArchetypeData.Id) { }

        #endregion
      }

      #endregion
    }

    #endregion

    public class Id : Archetype.LinkSystemComponent.Id {

      public Id(string name, string externalIdPrefixEnding = "") 
        : base(name, externalIdPrefixEnding) {}
    }

    /// <summary>
    /// For adding this to archetypes:
    /// </summary>
    /// <param name="type"></param>
    /// <param name="archetypeComponent"></param>
    public WeaponStatsLinkSystemComponent(
      ArchetypeData archetypeComponent,
      Type archetype = null
    ) : base(
          archetype ?? Type.Id.Archetype as Archetype.LinkSystemComponent.Type<
            WeaponStatsLinkSystemComponent,
            ModelData,
            ArchetypeData,
            ModelData.Type,
            ArchetypeData.Type
          >,
          archetypeComponent
        ) { }

    #region Type

    public class Type : Archetype.LinkSystemComponent.Type<
      WeaponStatsLinkSystemComponent,
      ModelData,
      ArchetypeData,
      ModelData.Type,
      ArchetypeData.Type
    > {

      public static WeaponStatsLinkSystemComponent.Id Id {
        get;
      } = new Id("Weapon Stat Data");

      #region System Logic

      /// THESE functions cannot be overriden by mods:

      /// <summary>
      /// Calculate damage total
      /// </summary>
      public static SimpleLogic<int> CalculateDamageTotal { get; }
        = (modelData, archetypeData)
          => modelData.damageBonus + archetypeData.BaseDamageValue;

      /// <summary>
      /// Calculate damage total
      /// </summary>
      public static AdvancedLogic<int> AdvancedCalculateDamageTotal { get; }
        = (modelData, archetypeData, model, archetype)
          => modelData.damageBonus + archetypeData.BaseDamageValue;

      /// <summary>
      /// Calculate damage total
      /// </summary>
      public static SimpleLogic IncreaseDamageBonus { get; }
        = (modelData, archetypeData)
          => modelData.damageBonus++;

      /// <summary>
      /// Calculate damage total
      /// </summary>
      public static AdvancedLogic AdvancedIncreaseDamageBonus { get; }
        = (modelData, archetypeData, model, archetype)
          => modelData.damageBonus++;

      public static AdvancedFunction<(Item Item, int TestValue), string> TestFunctionWithInput { get; }
        = (input, modelData, archetypeData, model, archetype)
          => input.TestValue.ToString() + input.Item.getUniqueId();

      /// THESE ones can:

      /// <summary>
      /// Calculate damage total
      /// </summary>
      public virtual SimpleLogic<int> ModdableCalculateDamageTotal { get; }
        = (modelData, archetypeData)
          => modelData.damageBonus + archetypeData.BaseDamageValue;

      #endregion

      #region Initialization

      /// <summary>
      /// This Ctor can be called by the singleton constructor at load, and can be called by sub-classes to make defaults.
      /// </summary>
      /// <param name="archetypeId"></param>
      protected Type(Id archetypeId = null)
        : base(archetypeId ?? Type.Id) { } 

      #endregion
    }

    #endregion
  }
}
