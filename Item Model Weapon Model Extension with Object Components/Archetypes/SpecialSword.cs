﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;
using System.Collections.Generic;

namespace Meep.Tech.Data.Examples {

  public class SpecialSword : Sword {

    public static Weapon.Id Id {
      get;
    } = new Weapon.Id("SpecialSword", "Weapons.");

    SpecialSword() : base(Id) { }

    /// <summary>
    /// Replace a component with a modded version:
    /// </summary>
    public override OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent> DefaultSystemComponents
      => base.DefaultSystemComponents
          .Replace(
            // id of the base component to replace:
            WeaponStatsLinkSystemComponent.Type.Id,
            new WeaponStatsLinkSystemComponent(
              new WeaponStatsLinkSystemComponent.ArchetypeData(damage: 100),
              // also use the new modded type for the actual system logic:
              Archetype.GetInstance<WeaponDamageMultiplierModLinkType>()
            )
          )// OR (you DONT need both of these):
          .Update(
            // id of the base component to update:
            WeaponStatsLinkSystemComponent.Type.Id,
            (WeaponStatsLinkSystemComponent existingLinkSystem) => new WeaponStatsLinkSystemComponent(
              new WeaponStatsLinkSystemComponent.ArchetypeData(
                // you can use the base component to calculate the new damange this way:
                damage: existingLinkSystem.currentArchetypeDataComponent.BaseDamageValue * 10
              ),
              // also use the new modded type for the actual system logic:
              Archetype.GetInstance<WeaponDamageMultiplierModLinkType>()
            )
          );
  }
}
