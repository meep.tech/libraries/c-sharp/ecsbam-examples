﻿# Modability/Extension Examples: Archetypes
## Weapon.Type
### A New/Secondary Base Archetype
This is an extension of Item.Type, and can be used as a parent of all Weapon based Item Archetypes if one wishes.
The new keyword here distinguishes it from Item.Type.
This class is abstract, so it will not make a singleton of it's own, it is a new type of Base Archetype.
This type can be used to narrow requirements to only Archetypes for Weapon based items.

This Archetype is under the partial Model class "Weapon" in order to maintain the naming scheem Model.Type for base Archetype types.
Technically this is not a "Base Archetype System.Type", but a secondary "Abstract Child Archetype System.Type" as this type still relies on having an Item.Type for it's model's .type field.
  > If you wanted, you could add a "new" .type field to the model to account for this that gets set in it's inheriting ctor.

The constructor is protected and accepts just the Id to allow future Archetypes to extend this as Weapon.Type based Archetypes for weapon models.
This is standard for any abstract Archetype type, as we saw with Item.Type

Since all Archetypes extending Weapon.Type will need to use the weapon model, we need to override InitializeModel here.

To ovveride data in child Archetypes, we use property overrides.
Here we use DefaultSystemComponents to remove the StackQuantityStackableLink component; meaning all weapons are not stackable.
We also add the WeaponStatsLinkSystemComponent to contain weapon stats in all models and child Archetypes that inherit from this.

## Sword
### A simple Weapon.Type based Archetype.
This type is not abstract, and thus will try to be used to build an Archetype Singleton.
Sword is not an Abstract or Base Archetype, thus we don't need to keep up the model.Type naming scheem by making it a sub-class.

Like all archetypes this needs to impliment it's own Id.
Here we quickly add the id to the sub-domain "Weapons" as well, this could be done easier as we do in Item.Id using the externalIdPrefixEnding if we'd wanted to avoid repeated code and strings.

This archetype has a protected constructor that takes one argument.
This constructor is protected, not private, as we want to be able to extend this type in the future into new slightly different Archetypes.
Since we want this constructor to still produce a singleton with the correct ID, this constructor must default to the type Id we want (Sword.Id here) in case Null is passed in. (it will be)

This example provides two different ways to update an existing archetype using two variations of the Update function, you don't need to use both.

b4s_setBaseDamageValue is an example of a function that will throw an AttemptedUpdateWhileSealedException to prevent use after all Archetypes are loaded and sealed.
This can be useful if you want to allow mods to modify parts of an archetype before they're locked, but not after, or don't want to construct a whole new object to modify one detail of a component.

## SpecialSword
### A Weapon.Type that uses a Modded Link-System.
This type is not abstract, and thus will try to be used to build an Archetype Singleton.
SpecialSword is not an Abstract or Base Archetype, thus we don't need to keep up the model.Type naming scheem by making it a sub-class.

This archetype has a private construtor that takes no arguments, it cannot be extended into new Archetypes later on, or in mods.
This constructor will only be used once to make the Archetype singleton.

This type replaces the WeaponStatsLinkSystemComponent in a special way.
This replaces the Archetype used for the WeaponStatsLinkSystemComponent component, which should contain the Logic functions.
Some components will have logic functions that can be overriden. In this case, you can use the new child Archetype with the overriden functions instead.

Here we replace a base WeaponStatsLinkSystemComponent component with a WeaponStatsLinkSystemComponent using the WeaponDamageMultiplierModLinkType Archetype instead of the default: WeaponStatsLinkSystemComponent.Type
This new Archetype overloads how damage is calculated whenever it's grabbed and called.
Here we also provide two ways to replace the existing Archetype, you do NOT need both. You can use EITHER Replace, or Update for each individual component, depending on your needs.
Both Update and Replace can be used for different components on the same Archetype though depending on needs.