﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Static;
using System.Collections.Generic;

namespace Meep.Tech.Data.Examples {
  public class Sword : Weapon.Type {

    public static Weapon.Id Id {
      get;
    } = new Weapon.Id("Sword", "Weapons.");

    protected Sword(Weapon.Id id) 
      : base(id ?? Sword.Id) { }

    /// <summary>
    /// Update the weapon damage of a sword to 10.
    /// </summary>
    public override OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent> DefaultSystemComponents
      => base.DefaultSystemComponents
        .Update(WeaponStatsLinkSystemComponent.Type.Id, (WeaponStatsLinkSystemComponent weaponStatsLinkSystemComponent)
          => new WeaponStatsLinkSystemComponent(
            new WeaponStatsLinkSystemComponent.ArchetypeData(
              damage: 10
            )
          )
        ) // OR (you DO NOT need both of these): if you can update elements of the archetype with a function before sealing for example:
        .Update(WeaponStatsLinkSystemComponent.Type.Id, (WeaponStatsLinkSystemComponent weaponStatsLinkSystemComponent) => {
          weaponStatsLinkSystemComponent.currentArchetypeDataComponent.b4s_setBaseDamageValue(10);

          return weaponStatsLinkSystemComponent;
        });
  }
}
