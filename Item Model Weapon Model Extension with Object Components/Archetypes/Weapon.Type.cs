﻿using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;

namespace Meep.Tech.Data.Examples {
  public partial class Weapon {
    public new abstract class Type : Item.Type {

      /// <summary>
      /// Remove one component, and add another:
      /// </summary>
      public override OrderdDictionary<Archetype.SystemComponent.Id, ISystemComponent> DefaultSystemComponents 
        => base.DefaultSystemComponents
          .Without(StackQuantityStackableLink.TypeId)
          .Append(WeaponStatsLinkSystemComponent.Type.Id,
            new WeaponStatsLinkSystemComponent(
              new WeaponStatsLinkSystemComponent.ArchetypeData()
          ));

      protected Type(Id id) : base(id) {}

      protected override Item InitializeModel(Model.Params @params)
        => new Weapon(this);
    }
  }
}


