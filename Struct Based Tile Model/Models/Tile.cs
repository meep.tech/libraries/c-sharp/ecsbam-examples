﻿using Meep.Tech.Data.Attributes.Models;
using Meep.Tech.Data.Extensions;

namespace Meep.Tech.Data.Examples {

  /// <summary>
  /// An item that could be used in a game:
  /// </summary>
  [DataModel("Tile", typeof(Tile.Type))]
  public partial struct Tile : IModel<Tile, Tile.Type, Tile.Id> {

    [ModelTypeField]
    public Type type { 
      get; 
    }

    /// <summary>
    /// Simple value for a tile
    /// </summary>
    [ModelDataField]
    public Type.Color color {
      get;
      private set;
    }

    /// <summary>
    /// Build an item of the given archetype.
    /// This should only be used in InitializeModel.
    /// </summary>
    /// <param name="archetype"></param>
    internal Tile(Tile.Type archetype) {
      type = archetype;
      color = Type.Color.Green;
    }
  }
}
