﻿namespace Meep.Tech.Data.Examples {
  public class Grass : Tile.Type {

    public static Tile.Id Id { 
      get;
    } = new Tile.Id("Grass");

    Grass() : base(Grass.Id) {}
  }
}
