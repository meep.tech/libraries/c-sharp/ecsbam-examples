﻿using Meep.Tech.Data.Base;
using System.Collections.Generic;

namespace Meep.Tech.Data.Examples {

  public partial struct Tile {

    /// <summary>
    /// The type collection to hold and register all the Item.Types
    /// </summary>
    public static Archetype.Collection<Tile, Tile.Type, Tile.Id> Types {
      get;
    } = new Archetype.Collection<Tile, Type, Id>();

    /// <summary>
    /// Doesn't need to be abstract
    /// </summary>
    public class Id : Archetype.Identity {
      internal Id(string name, string externalIdPrefixEnding = "")
        : base(name, $"Tiles.{externalIdPrefixEnding}") { }
    }

    /// <summary>
    /// The base Archetype Type class for Tile Models.
    /// All Tile models must be made with a subtype of this Type as it's "type" value
    /// </summary>
    public abstract class Type : Archetype<Tile, Tile.Type, Tile.Id> {

      /// <summary>
      /// Color for a simple value
      /// </summary>
      public enum Color {
        Green,
        Blue,
        Red
      }

      /// <summary>
      /// Params for new types of params for tile.types
      /// </summary>
      public class Params : Param {

        public static Params Color { get; } = new Params("Color", typeof(Color));

        protected Params(string name, System.Type type, string externalIdEnding = "") 
          : base(name, type, $"Tile.{externalIdEnding}") {}
      }

      /// <summary>
      /// Used for making new Tile.Types
      /// </summary>
      /// <param name="id"></param>
      protected Type(Tile.Id id)
        : base(id, Tile.Types) {}

      /// <summary>
      /// Set up the constructor for the model:
      /// </summary>
      protected override Tile InitializeModel(Model.Params @params)
        => new Tile(this);
    }
  }
}
